name := "tcp_server"

version := "0.1"

scalaVersion := "2.13.1"

mainClass in (Compile, assembly) := Some("com.collibra.Server")

libraryDependencies ++= Seq(
  "org.jgrapht" % "jgrapht-core" % "1.3.0",
  "org.jheaps" % "jheaps" % "0.10",
  "org.scalactic" %% "scalactic" % "3.1.0",
  "org.scalatest" %% "scalatest" % "3.1.0" % "test",
)