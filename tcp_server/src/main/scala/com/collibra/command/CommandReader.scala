package com.collibra.command

import java.io.{BufferedReader, InputStreamReader}
import java.net.{SocketException, SocketTimeoutException}

import com.collibra.client.Client

import scala.util.{Failure, Success, Try}

class CommandReader(client: Client) {
  private val in = new BufferedReader(new InputStreamReader(client.socket.getInputStream))

  def read(): Command = {
    Try {
      val line = in.readLine()
      line.split(" ").toList
    } match {
      case Success(commandList) => commandList match {
          case "HI," :: "I" :: "AM" :: name :: Nil => Hi(client, name)
          case "BYE" :: "MATE!" :: Nil => Bye(client)
          case "ADD" :: "NODE" :: node :: Nil => AddNode(client, node)
          case "REMOVE" :: "NODE" :: node :: Nil => RemoveNode(client, node)
          case "ADD" :: "EDGE" :: nodeX :: nodeY :: weight :: Nil => AddEdge(client, nodeX, nodeY, weight.toInt)
          case "REMOVE" :: "EDGE" :: nodeX :: nodeY :: Nil => RemoveEdge(client, nodeX, nodeY)
          case "SHORTEST" :: "PATH" :: nodeX :: nodeY :: Nil => ShortestPath(client, nodeX, nodeY)
          case "CLOSER" :: "THAN" :: weight :: nodeX :: Nil => CloserThan(client, weight.toInt, nodeX)
          case _ => Unknown(client)
      }
      case Failure(exception) => exception match {
        case e: SocketTimeoutException =>
          println(e.getMessage)
          Bye(client)
        case e: SocketException =>
          println(e.getMessage)
          Close(client)
      }
    }
  }
}
