package com.collibra.command

import com.collibra.client.Client

abstract class Command() {}

case class Hi(client: Client, name: String) extends Command() {}
case class Bye(client: Client) extends Command() {}
case class Unknown(client: Client) extends Command() {}
case class Close(client: Client) extends Command() {}

case class AddNode(client: Client, node: String) extends Command() {}
case class RemoveNode(client: Client, node: String) extends Command() {}
case class AddEdge(client: Client, nodeX: String, nodeY: String, weight: Int) extends Command() {}
case class RemoveEdge(client: Client, nodeX: String, nodeY: String) extends Command() {}

case class ShortestPath(client: Client, nodeX: String, nodeY: String) extends Command() {}
case class CloserThan(client: Client, weight: Int, nodeX: String) extends Command() {}