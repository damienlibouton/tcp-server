package com.collibra.command

import org.jgrapht.Graph
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import org.jgrapht.graph.DefaultWeightedEdge
import org.jgrapht.graph.builder.GraphTypeBuilder
import org.jgrapht.traverse.ClosestFirstIterator

import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters._

object CommandHandler {
  val graph: Graph[String, DefaultWeightedEdge] = GraphTypeBuilder
    .directed[String, DefaultWeightedEdge]
    .allowingMultipleEdges(true)
    .allowingSelfLoops(true)
    .edgeClass(classOf[DefaultWeightedEdge])
    .weighted(true).buildGraph

  def handle(command: Command): Unit = {
    command match {
      case Hi(client, name) =>
        client.name = name
        client.sayWelcome()
      case Bye(client) =>
        client.sayBye()
        client.close()
      case Unknown(client) =>
        client.saySorry()
      case Close(client) =>
        println(s"Client ${client.name} leaves us")
        client.close()

      case AddNode(client, node) =>
        graph.synchronized {
          if (graph.addVertex(node)) client.send("NODE ADDED")
          else client.send("ERROR: NODE ALREADY EXISTS")
        }
      case RemoveNode(client, node) =>
        graph.synchronized {
          if (graph.removeVertex(node)) client.send("NODE REMOVED")
          else client.send("ERROR: NODE NOT FOUND")
        }

      case AddEdge(client, nodeX, nodeY, weight) =>
        graph.synchronized {
          if (graph.containsVertex(nodeX) && graph.containsVertex(nodeY)) {
            val edge = new DefaultWeightedEdge
            graph.addEdge(nodeX, nodeY, edge)
            graph.setEdgeWeight(edge, weight)
            client.send("EDGE ADDED")
          }
          else client.send("ERROR: NODE NOT FOUND")
        }
      case RemoveEdge(client, nodeX, nodeY) =>
        graph.synchronized {
          Option(graph.removeAllEdges(nodeX, nodeY).asScala) match {
            case Some(_) => client.send("EDGE REMOVED")
            case None => client.send("ERROR: NODE NOT FOUND")
          }
        }

      case ShortestPath(client, nodeX, nodeY) =>
        graph.synchronized {
          if (graph.containsVertex(nodeX) && graph.containsVertex(nodeY)) {
            val dijkstraAlg = new DijkstraShortestPath[String, DefaultWeightedEdge](graph)
            val xPaths = dijkstraAlg.getPaths(nodeX)
            Option(xPaths.getPath(nodeY)) match {
              case Some(shortestPath) => client.send(s"${shortestPath.getWeight.toInt}")
              case None => client.send(s"${Integer.MAX_VALUE}")
            }
          }
          else client.send("ERROR: NODE NOT FOUND")
        }

      case CloserThan(client, weight, nodeX) =>
        graph.synchronized {
          if (graph.containsVertex(nodeX)) {
            val iterator: ClosestFirstIterator[String, DefaultWeightedEdge] = new ClosestFirstIterator(graph, nodeX, (weight - 1).toDouble)
            val nodes = new ListBuffer[String]()
            iterator.forEachRemaining((node: String) => if(node != nodeX) nodes += node)
            val nodesCommaSeparated: String = nodes.sorted.mkString(",")
            client.send(nodesCommaSeparated)
          }
          else client.send("ERROR: NODE NOT FOUND")
        }
    }
  }

  def clearGraph(graph: Graph[String, DefaultWeightedEdge]): Unit = {
    graph.synchronized {
      val edges = new ListBuffer[DefaultWeightedEdge]
      graph.edgeSet().asScala.foreach(e => edges += e)
      graph.removeAllEdges(edges.asJavaCollection)

      val nodes = new ListBuffer[String]
      graph.vertexSet().asScala.foreach(n => nodes += n)
      graph.removeAllVertices(nodes.asJavaCollection)
    }
  }
}
