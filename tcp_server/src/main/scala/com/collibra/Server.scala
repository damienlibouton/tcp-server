package com.collibra

import java.net.ServerSocket
import java.util.concurrent.Executors

import com.collibra.client.ClientHandler

import scala.util.Using

object Server extends App {
  private val PORT = 50000
  private val WORKERS = 100
  private val pool = Executors.newFixedThreadPool(WORKERS)

  println("Server is running")
  Using(new ServerSocket(PORT)) {
    listener => {
      while (true) {
        println(s"$listener is waiting a client")
        pool.execute(new ClientHandler(listener.accept, System.currentTimeMillis))
      }
    }
  }
}
