package com.collibra.client

import java.net.Socket
import java.util.UUID

import com.collibra.command.{Command, CommandHandler, CommandReader}

class ClientHandler(clientSocket: Socket, acceptedAt: Long) extends Runnable {
  private val socketTimeout: Int = 30000
  private val sessionId: String = UUID.randomUUID.toString
  val client = new Client(clientSocket, sessionId, socketTimeout)
  val commandReader = new CommandReader(client)

  override def run(): Unit =  {
    client.sayHi()
    while (client.isConnected) commandReader.read() match {
      case c: Command => CommandHandler.handle(c)
    }
  }
}
