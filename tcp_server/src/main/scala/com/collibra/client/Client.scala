package com.collibra.client

import java.io.PrintWriter
import java.net.Socket

class Client(val socket: Socket, sessionId: String, timeout: Int) {
  socket.setSoTimeout(timeout)
  private val out: PrintWriter = new PrintWriter(socket.getOutputStream, true)
  private val acceptedAt = System.currentTimeMillis()
  var isConnected = true
  var name = "???"

  def sayHi(): Unit = send(s"HI, I AM $sessionId")
  def sayBye(): Unit = send(s"BYE $name, WE SPOKE FOR ${elapsedTime()} MS")
  def sayWelcome(): Unit = send(s"HI $name")
  def saySorry(): Unit = send("SORRY, I DID NOT UNDERSTAND THAT")

  def close(): Unit = {
    isConnected = false
    out.close()
    socket.close()
  }

  def send(msg: String): Unit = {
    out.println(msg)
  }

  private def elapsedTime(): Long = {
    val now = System.currentTimeMillis()
    now - acceptedAt
  }
}
