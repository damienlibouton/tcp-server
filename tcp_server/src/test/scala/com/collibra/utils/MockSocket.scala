package com.collibra.utils

import java.io.{ByteArrayInputStream, InputStream, OutputStream}
import java.net.Socket
import java.nio.charset.StandardCharsets

import scala.collection.mutable.ArrayBuffer

class MockSocket() extends Socket {
  var bytesList = new ArrayBuffer[Byte]()
  var msg = ""

  def output(): String = {
    new String(bytesList.toArray, StandardCharsets.UTF_8)
  }

  def clearOutput(): Unit = {
    bytesList = new ArrayBuffer[Byte]()
  }

  override def getInputStream: InputStream = {
    new ByteArrayInputStream(s"$msg".getBytes)
  }

  override def getOutputStream: OutputStream = {
    b: Int => {
      bytesList += b.toByte
    }
  }
}
