package com.collibra

import com.collibra.client.Client
import com.collibra.command._
import com.collibra.utils.MockSocket
import org.jgrapht.graph.DefaultWeightedEdge
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

class CommandHandlerTest extends AnyFunSuite with BeforeAndAfterEach {
  import com.collibra.command.CommandHandler._

  var mockSocket: MockSocket = _
  var client: Client = _

  override def beforeEach(): Unit = {
    mockSocket = new MockSocket()
    client = new Client(mockSocket, "my_id", 1000)
    clearGraph(graph)
  }

  def graphFixture(): Unit = {
    graph.addVertex("Mark")
    graph.addVertex("Michael")
    graph.addVertex("Madeleine")
    graph.addVertex("Mufasa")
    graph.addVertex("John")
    val edge1 = new DefaultWeightedEdge
    val edge2 = new DefaultWeightedEdge
    val edge3 = new DefaultWeightedEdge
    val edge4 = new DefaultWeightedEdge
    graph.addEdge("Mark", "Michael", edge1)
    graph.addEdge("Michael", "Madeleine", edge2)
    graph.addEdge("Madeleine", "Mufasa", edge3)
    graph.addEdge("Mark", "Madeleine", edge4)
    graph.setEdgeWeight(edge1, 5)
    graph.setEdgeWeight(edge2, 2)
    graph.setEdgeWeight(edge3, 8)
    graph.setEdgeWeight(edge4, 14)
  }

  test("clearGraph should clear an empty graph") {
    assert(!graph.containsVertex("X"))
    assert(!graph.containsVertex("Y"))
    assert(!graph.containsEdge("X","Y"))
  }

  test("clearGraph should remove all nodes and edges") {
    graph.addVertex("X")
    graph.addVertex("Y")
    val edge = new DefaultWeightedEdge
    graph.addEdge("X", "Y", edge)
    graph.setEdgeWeight(edge, 1)

    clearGraph(graph)

    assert(!graph.containsVertex("X"))
    assert(!graph.containsVertex("Y"))
    assert(!graph.containsEdge(edge))
  }

  test("handle(Hi) should assign a name to the client") {
    handle(Hi(client, "foo"))
    assert(client.name == "foo")
  }

  test("handle(Hi) should send 'HI <client_name>\n'") {
    handle(Hi(client, "foo"))
    val output = mockSocket.output()
    assert(output == "HI foo\n")
  }

  test("handle(Bye) should send 'BYE <client_name>, WE SPOKE FOR <elapsed_time> MS\n'") {
    client.name = "foo"
    handle(Bye(client))
    val output = mockSocket.output()
    assert(output.startsWith("BYE foo, WE SPOKE FOR "))
    assert(output.endsWith(" MS\n"))
  }

  test("handle(Bye) should close the client") {
    handle(Bye(client))
    assert(!client.isConnected)
  }

  test("handle(Unknown) should send 'SORRY, I DID NOT UNDERSTAND THAT\n'") {
    handle(Unknown(client))
    val output = mockSocket.output()
    assert(output == "SORRY, I DID NOT UNDERSTAND THAT\n")
  }

  test("handle(AddNode(X)) should send 'NODE ADDED\n' if X doesn't exist") {
    handle(AddNode(client, "X"))
    val output = mockSocket.output()
    assert(output == "NODE ADDED\n")
  }

  test("handle(AddNode(X)) should send 'ERROR: NODE ALREADY EXISTS\n' if X exists") {
    graph.addVertex("X")
    handle(AddNode(client, "X"))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE ALREADY EXISTS\n")
  }

  test("handle(RemoveNode(X)) should send 'NODE REMOVED\n' if X exists") {
    graph.addVertex("X")
    handle(RemoveNode(client, "X"))
    val output = mockSocket.output()
    assert(output == "NODE REMOVED\n")
  }

  test("handle(RemoveNode(X)) should send 'ERROR: NODE NOT FOUND\n' if X doesn't exist") {
    handle(RemoveNode(client, "X"))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE NOT FOUND\n")
  }

  test("handle(AddEdge(X, Y, 1)) should send 'EDGE ADDED\n' if X and Y exist") {
    graph.addVertex("X")
    graph.addVertex("Y")
    handle(AddEdge(client, "X", "Y", 1))
    val output = mockSocket.output()
    assert(output == "EDGE ADDED\n")
  }

  test("handle(AddEdge(X, Y, 1)) should send 'ERROR: NODE NOT FOUND\n' if X or Y, doesn't exist") {
    handle(AddEdge(client, "X", "Y", 1))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE NOT FOUND\n")
  }

  test("handle(RemoveEdge(X, Y)) should send 'EDGE REMOVED\n' if X and Y exist") {
    graph.addVertex("X")
    graph.addVertex("Y")
    val edge = new DefaultWeightedEdge
    graph.addEdge("X", "Y", edge)
    graph.setEdgeWeight(edge, 1)
    handle(RemoveEdge(client, "X", "Y"))
    val output = mockSocket.output()
    assert(output == "EDGE REMOVED\n")
  }

  test("handle(RemoveEdge(X, Y)) should send 'ERROR: NODE NOT FOUND\n' if X or Y, doesn't exist") {
    handle(RemoveEdge(client, "X", "Y"))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE NOT FOUND\n")
  }

  test("handle(ShortestPath(Mark, Madeleine)) should send the '7\n'") {
    graphFixture()
    handle(ShortestPath(client, "Mark", "Madeleine"))
    val output = mockSocket.output()
    assert(output == "7\n")
  }

  test("handle(ShortestPath(Mark, John)) should send the 'Integer.MAX_VALUE\n'") {
    graphFixture()
    handle(ShortestPath(client, "Mark", "John"))
    val output = mockSocket.output()
    assert(output == s"${Integer.MAX_VALUE}\n")
  }

  test("handle(ShortestPath(X, John)) should send the 'ERROR: NODE NOT FOUND\n'") {
    graphFixture()
    handle(ShortestPath(client, "X", "John"))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE NOT FOUND\n")
  }

  test("handle(CloserThan(8, Mark)) should send the 'Madeleine,Michael\n'") {
    graphFixture()
    handle(CloserThan(client, 8, "Mark"))
    val output = mockSocket.output()
    assert(output == "Madeleine,Michael\n")
  }

  test("handle(CloserThan(8, X)) should send the 'ERROR: NODE NOT FOUND\n'") {
    graphFixture()
    handle(CloserThan(client, 8, "X"))
    val output = mockSocket.output()
    assert(output == "ERROR: NODE NOT FOUND\n")
  }
}
