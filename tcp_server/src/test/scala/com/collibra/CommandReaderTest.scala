package com.collibra

import java.net.{ServerSocket, Socket}

import com.collibra.client.Client
import com.collibra.command._
import com.collibra.utils.MockSocket
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite


class CommandReaderTest extends AnyFunSuite with BeforeAndAfterEach {
  var client: Client = _
  var mockSocket: MockSocket = _

  override def beforeEach(): Unit = {
    mockSocket = new MockSocket()
    client = new Client(mockSocket, "my_id", 1000)
  }

  test("CommandReader.read() without input should timeout and return a Bye command") {
    val server = new ServerSocket(9999)
    val socket = new Socket("localhost", 9999)
    val client = new Client(socket, "my_id", 1000)
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    socket.close()
    server.close()
    assert(command.isInstanceOf[Bye])
  }

  test("??? -> should return an Unknown command") {
    mockSocket.msg = "???"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[Unknown])
  }

  test("HI, I AM Foo -> should return a Hi command") {
    mockSocket.msg = "HI, I AM Foo"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[Hi])
  }

  test("BYE MATE! should return a Bye command") {
    mockSocket.msg = "BYE MATE!"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[Bye])
  }

  test("ADD NODE X should return a AddNode command") {
    mockSocket.msg = "ADD NODE X"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[AddNode])
  }

  test("REMOVE NODE X should return a RemoveNode command") {
    mockSocket.msg = "REMOVE NODE X"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[RemoveNode])
  }

  test("ADD EDGE X Y 1 should return a AddEdge command") {
    mockSocket.msg = "ADD EDGE X Y 1"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[AddEdge])
  }

  test("REMOVE EDGE X Y should return a RemoveEdge command") {
    mockSocket.msg = "REMOVE EDGE X Y"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[RemoveEdge])
  }

  test("SHORTEST PATH X Y should return a ShortestPath command") {
    mockSocket.msg = "SHORTEST PATH X Y"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[ShortestPath])
  }

  test("CLOSER THAN 1 X should return a CloserThan command") {
    mockSocket.msg = "CLOSER THAN 1 X"
    val commandReader = new CommandReader(client)
    val command = commandReader.read()
    assert(command.isInstanceOf[CloserThan])
  }
}
