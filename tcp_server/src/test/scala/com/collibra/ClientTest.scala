package com.collibra

import com.collibra.client.Client
import com.collibra.utils.MockSocket
import org.scalatest.BeforeAndAfterEach
import org.scalatest.funsuite.AnyFunSuite

class ClientTest extends AnyFunSuite with BeforeAndAfterEach {
  var client: Client = _
  var mockSocket: MockSocket = _

  override def beforeEach(): Unit = {
    mockSocket = new MockSocket()
    client = new Client(mockSocket, "my_id", 1000)
  }

  test("sayHi() should send 'HI, I AM my_id\n'") {
    client.sayHi()
    val output = mockSocket.output()
    assert(output == "HI, I AM my_id\n")
  }

}
